import express from 'express';
import bodyParser from 'body-parser';

export const router = express.Router();
export default { router };

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


router.get('/', (req, res) => {
    res.render('index');
});

router.get('/pago', (req, res) => {
    const params = {
        numero: req.query.numero,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        servicio: req.query.tipo,
        kwConsumidos: req.query.kw,
        data: false,
    }

    res.render('pago',{ params: params });
});

router.post('/pago', (req, res) => {
    const numero = req.body.numero;
    const nombre = req.body.nombre;
    const tipo = req.body.tipo;
    const kwatts = req.body.kw;
    let costoKwatts;
    let subTotal;
    let descuento;
    let impuesto;
    let total;

    switch (tipo) {
        case 'Domestico':
            costoKwatts = 1.08;
            break;
        case 'Comercial':
            costoKwatts = 2.5;
            break;
        case 'Industrial':
            costoKwatts = 3.0;
            break;
        default:
            // En caso de que el tipo de servicio no coincida con ninguno de los casos anteriores
            // Puedes agregar un mensaje de error o un valor predeterminado
            res.status(400).send('Tipo de servicio no válido');
            return;
    }

    subTotal = costoKwatts * kwatts;
    impuesto = (subTotal * 0.16).toFixed(2);

    if (kwatts <= 1000) {
        descuento = (subTotal * 0.10).toFixed(2);
    } else if (kwatts > 1000 && kwatts <= 10000) {
        descuento = (subTotal * 0.20).toFixed(2);
    } else if (kwatts > 10000) {
        descuento = (subTotal * 0.50).toFixed(2);
    }

    total = (subTotal + parseFloat(impuesto)) - parseFloat(descuento);

    const params = {
        numero,
        nombre,
        tipo,
        kwatts,
        costoKwatts,
        subTotal: subTotal.toFixed(2),
        descuento,
        impuesto,
        total: total.toFixed(2),
        data: true
    };
    console.log(`El sub total es: ${params.subTotal}`);
    console.log(params);

    res.render('pago', { params: params });
});

